#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    char str[33];
    // Hash the guess using MD5
    strcpy(str, md5(guess,strlen(guess)));
    // Compare the two hashes
    int result = strcmp(hash,str);
    // Free any malloc'd memory
    //free(str);
    if(result == 0) return 1;
    else return 0;
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    FILE *f = fopen(filename, "r");
    if(!f) printf("can't open %s for reading\n", filename);
    
    char str[100];
    
    //get length of file being passed in then rewind to beginning
    int size=0;
    while(fscanf(f, "%s\n", str) != EOF)
    {
        size++;
    }
    rewind(f);
    
    //allocate memory for structure based on size of file
    char **result;
    result = malloc(sizeof(char *) * (size));
    //allocate memory for each string
    for(int i = 0; i<size; i++) {
        result[i] = malloc(HASH_LEN*sizeof(char));
    }
    //scan through file addding each line to result array
    int index = 0;
    while(fscanf(f, "%s\n", str) != EOF)
    {
        strcpy(result[index], str);  
        index++;
    }
    //close file then append null character to resulting array
    fclose(f);
    strcpy(result[index-1], "\0");
    
    return result;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);

    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]);

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    for(int i =0; hashes[i] != NULL; i++) {
        for(int k =0; dict[k] != NULL; k++) {
            if(tryguess(hashes[i], dict[k]) == 1) printf("%s  %s\n", hashes[i], dict[k]);         
        }
    }
            
}
